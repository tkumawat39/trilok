<!DOCTYPE html>
<html lang="en">
<?php

require("language/language.php");
require("includes/function.php");
include("includes/head.php");

?>
<!-- Theme JS files -->
<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/js/core/app.js"></script>
<script type="text/javascript" src="assets/js/pages/form_inputs.js"></script>
<!-- /theme JS files -->
<body class="navbar-bottom">

<!-- Main navbar -->
<?php 
include("includes/header.php");


 $userId = $_SESSION['id'];
$is_SuerAdmin = "SELECT is_superadmin FROM tbl_admin WHERE id = ".$userId."";
$superAdmin = mysqli_fetch_array(mysqli_query($mysqli,$is_SuerAdmin));


if($superAdmin['is_superadmin'] == 1){ 
	$isSuperAdmin = true;
} else {
	
	header( "Location:home.php");
exit;
}

require_once("thumbnail_images.class.php");

 
if(isset($_POST['submit']) )
{		$error =array();
	 	$userNamequery="SELECT id FROM tbl_admin WHERE username = '".$_POST['username']."'";		
		$sql = mysqli_query($mysqli,$userNamequery);
				$resultQuery=mysqli_fetch_assoc($sql);
				//print_r($resultQuery); 
		if(isset($resultQuery) && !empty($resultQuery)){			
			$error[] = 'User name Already exists';
			
		}
		
		
		$userEmailquery="SELECT id FROM tbl_admin WHERE email = '".$_POST['email']."'";		
		$sqlemail = mysqli_query($mysqli,$userEmailquery);
		$resultQueryemail=mysqli_fetch_assoc($sqlemail);
		if(isset($resultQueryemail) && !empty($resultQueryemail)){			
			$error[] = 'Email address Already exists';
			
		}
		
	/* echo '<pre>';
		print_r($error);
			die; */
		
		if(isset($error) && empty($error)) { //echo "faskjhfkajhk"; die;
		$profile_image=rand(0,99999)."_".$_FILES['profile_image']['name'];
       
       //Main Image
		$tpath1='imagess/'.$profile_image;        
		$pic1=compress_image($_FILES["profile_image"]["tmp_name"], $tpath1, 80);
  				
       $data = array( 
         'full_name'  =>  $_POST['full_name'],
		 'username'  =>  $_POST['username'],
		 'email'  =>  $_POST['email'],
		  'password'  =>  $_POST['password'],
		  'phone'  =>  $_POST['phone'],		  
		   'image'  =>  $profile_image
         
          );    

    $qry = Insert('tbl_admin',$data);  
 
    $_SESSION['msg']="10";
 
    header( "Location:manage_subadminusers.php");
    exit; 
		} 

  }
  
 /*  if(isset($_GET['cat_id']))
  {
       
      $qry="SELECT * FROM tbl_promo where id='".$_GET['cat_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

  } */
  /*if(isset($_POST['submit']) and isset($_POST['cat_id']))
  {
		if($_FILES['banner_image']['name']!="")
		{    


			$img_res=mysqli_query($mysqli,'SELECT * FROM tbl_banner_ad WHERE id='.$_GET['cat_id'].'');
			$img_res_row=mysqli_fetch_assoc($img_res);
      

          if($img_res_row['banner_image']!="")
            {
                unlink('imagess/'.$img_res_row['banner_image']);
           }

           $banner_image=rand(0,99999)."_".$_FILES['banner_image']['name'];
       
             //Main Image
           $tpath1='imagess/'.$banner_image;        
             $pic1=compress_image($_FILES["banner_image"]["tmp_name"], $tpath1, 80);
			 
           $data = array(
                 'promo_code'  =>  $_POST['promo_code'],
				 'minimum_value'  =>  $_POST['minimum_value'],
				 'promo_percentage'  =>  $_POST['promo_percentage'],
				  'promo_title'  =>  $_POST['promo_title'],
				  'promo_desc'  =>  $_POST['promo_desc'],
				   'promo_policy'  =>  $_POST['promo_policy'],
				   'promo_start_date'  =>  $_POST['promo_start_date'],
		            'promo_end_date'  =>  $_POST['promo_end_date'],
					'banner_image'  =>  $banner_image
					);  
 
               $category_edit=Update('tbl_promo', $data, "WHERE id = '".$_POST['cat_id']."'");
		}  
		else
     {

           $data = array(
				 'promo_code'  =>  $_POST['promo_code'],
				 'minimum_value'  =>  $_POST['minimum_value'],
				 'promo_percentage'  =>  $_POST['promo_percentage'],
				  'promo_title'  =>  $_POST['promo_title'],
				  'promo_desc'  =>  $_POST['promo_desc'],
				   'promo_policy'  =>  $_POST['promo_policy'],
				   'promo_start_date'  =>  $_POST['promo_start_date'],
		            'promo_end_date'  =>  $_POST['promo_end_date'],
				 
            );  
 
               $category_edit=Update('tbl_promo', $data, "WHERE id = '".$_POST['cat_id']."'");
     }

     
    $_SESSION['msg']="11"; 
    header( "Location:add_subuser.php");
    exit;
 }

 if(isset($_GET['cat_id']))
{
   
  $qry="SELECT * FROM tbl_promo where id='".$_GET['cat_id']."'";
  $result=mysqli_query($mysqli,$qry);
  $row=mysqli_fetch_assoc($result);

} */
?>
<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="home.php"><i class="icon-home2 position-left"></i> Home</a></li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold">Add </span> - sub user</h4>
			</div>

			<div class="heading-elements">
				<div class="heading-btn-group">
					<a href="manage_subadminusers.php" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i><span>Back</span></a>
				</div>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
					<?php include("includes/sidebar.php");?>
					<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Form horizontal -->
				<div class="panel panel-flat">
					
					<div class="panel-body">
						<?php if(isset($_SESSION['msg'])){?>
						<div class="alert alert-success no-border">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><?php echo $client_lang[$_SESSION['msg']] ; ?></span> 
						</div>
						<?php unset($_SESSION['msg']);}?>
						<?php if(isset($error) && !empty($error)){
							foreach($error as $errorDetails) {
							?>
						<div class="alert alert-success alert-error no-border">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><?php echo $errorDetails ; ?></span> 
						</div>
							<?php  } }?>
						
						
						<form class="form-horizontal" action="" name="addsubuser" method="post" enctype="multipart/form-data">
							<input  type="hidden" name="cat_id" value="<?php echo $_GET['cat_id'];?>" />
							<fieldset class="content-group">
								<legend class="text-bold">Sub user Detail</legend>

								<div class="form-group">
									<label class="control-label col-lg-2">Full name :-</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" name="full_name" id="full_name" value="<?php echo $_POST['full_name'];  ?>" placeholder="Enter Full Name" required>
									</div>
								</div>
		                      <div class="form-group">
						<label class="control-label col-lg-2">
						  User Name
						</label>
					   <div class="col-lg-10">
						  <input type="text" class="form-control m-input" name="username" id="username" value="<?php echo $_POST['username'];  ?>" placeholder="Enter Username" required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-lg-2">
						  Password   
						</label> 
					   <div class="col-lg-10">
						  <input type="password" class="form-control m-input" name="password" id="password" value="<?php echo $_POST['password'];  ?>" placeholder="Enter Password" required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-lg-2">
						  Email
						</label>
					   <div class="col-lg-10">
						  <input type="email" class="form-control m-input" name="email" id="email" value="<?php echo $_POST['email'];  ?>" placeholder="Enter Email" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-2">
						  Phone
						</label>
					   <div class="col-lg-10">
						 <input type="text" class="form-control m-input" name="phone" id="phonePhone number" value="<?php echo $_POST['phone'];  ?>" placeholder="Phone number" required>
						
						</div>
					</div>
				
				
				
					<div class="form-group">
									<label class="control-label col-lg-2"> Profile Image :-</label>
									<div class="col-lg-10">							
										<div class="media no-margin-top">
											<div class="media-left">
												<div class="media-body">
													<input type="file" id="profile_image" name="profile_image" class="file-styled-primary">
													</br>
												   <?php if( $row['profile_image']!="") {?>
													<img src="imagess/<?php echo $row['profile_image'];?>" style="width: 200px; height: 90px;" class="img-rounded" alt="">
													<?php } else{?>
													<img src="assets/images/placeholder.jpg" style="width: 75px; height: 75px;" class="img-rounded" alt=""/>                                   
													<?php }?>
													<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</fieldset>
							<div class="text-right">
								<button type="submit" name="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</form>
					</div>
				</div>
				<!-- /form horizontal -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	<?php include("includes/footer.php");?>
	<!-- /footer -->

</body>
</html>
